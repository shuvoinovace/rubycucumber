Given(/^We navigate to the Google WebSite$/) do
  visit '/'
end

When(/^We search for the "([^"]*)"$/) do |arg1|
  fill_in 'q', with: arg1
 
end

Then(/^The results for the search will be displayed$/) do
  click_button 'btnK'
end
